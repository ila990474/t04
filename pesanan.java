class pesanan extends kue{
    private double berat;
  

    public pesanan(String name, int price, int berat) {
        super(name, price); ////memanggil konstruktor pada kelas induk yaitu kelas kue
        this.berat = berat; //nilai dari atribut berat diatur dengan nilai dari parameter berat
    }

    public double getBerat() {
        return berat; ////mengembalikan nilai dari atribut berat
    }
    public void setBerat(double berat) { ////mengatur nilai dari atribut
        this.berat = berat;
    }

   

    @Override ////menandai method yang didefinisikan pada subclass merupakan pengganti dari method dengan nama yang sama pada superclass.
    public double hitungHarga() {
        return super.getPrice() * berat; ////mengembalikan hasil perkalian antara hargadan jumlah
    }

    @Override
    public double Berat() { ///mengembalikan nilai dari metode getBerat()
        return getBerat();
    }
    @Override
    public double Jumlah() {
       return 0;
    }
     
}