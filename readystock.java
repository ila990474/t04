class readystock extends kue{
    private double jumlah;

    public readystock(String name, double price, double jumlah) {
        super(name, price); //memanggil konstruktor pada kelas induk yaitu kelas kue
        this.jumlah=jumlah; //nilai dari atribut jumlah diatur dengan nilai dari parameter jumlah
    }

    public double getJumlah() { 
        return jumlah; //mengembalikan nilai dari atribut jumlah
    }

    public void setJumlah(double jumlah) { //mengatur nilai dari atribut
        this.jumlah = jumlah;  
    }

    @Override //menandai method yang didefinisikan pada subclass merupakan pengganti dari method dengan nama yang sama pada superclass
    public double hitungHarga() { 
        return super.getPrice() * jumlah*2;} //mengembalikan hasil perkalian antara harga, jumlah, dan 2

    @Override
    public double Berat() {
        return 0;
    }

    @Override
    public double Jumlah() {
        return getJumlah(); //mengembalikan nilai dari metode getJumlah()
        
    }

	
}